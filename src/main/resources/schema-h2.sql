CREATE TABLE PRODUCT
        (
        id NUMBER(10) NOT NULL,
        name VARCHAR2(50) NOT NULL,
        shortDescription VARCHAR2(50) NOT NULL,
        description VARCHAR2(200) NOT NULL,
        price NUMBER(10) NOT NULL,
        images VARCHAR2(1000) NOT NULL,
        stars NUMBER(10) NOT NULL,
        );