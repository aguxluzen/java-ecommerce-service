package ar.com.agustinluzenti.ecommerce.service;

import ar.com.agustinluzenti.ecommerce.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductService extends JpaRepository<Product, Integer>{
}