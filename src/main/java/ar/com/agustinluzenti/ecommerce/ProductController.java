package ar.com.agustinluzenti.ecommerce;

import ar.com.agustinluzenti.ecommerce.service.ProductService;
import ar.com.agustinluzenti.ecommerce.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @RequestMapping(value = "/product", method = RequestMethod.GET)
    Product getProduct(@RequestParam Integer id) {
        return productService.getOne(id);
    }

    @RequestMapping(value = "/Product", method = RequestMethod.POST)
    String addProduct(@RequestBody Product Product) {
        Product savedProduct = productService.save(Product);
        if (savedProduct != null) {
            return "SUCESS";
        } else {
            return "TOMAL";
        }
    }

    @RequestMapping(value = "/Product", method = RequestMethod.PUT)
    Product updateProduct(@RequestBody Product Product) {
        Product updatedProduct = productService.save(Product);
        return updatedProduct;
    }

    @RequestMapping(value = "/Product", method = RequestMethod.DELETE)
    Map deleteProduct(@RequestParam Integer id) {
        productService.deleteById(id);

        Map<String, String> status = new HashMap<>();
        status.put("Status", "Success");
        return status;
    }

    // Select, Insert, Delete for List of Products

    @RequestMapping(value = "/Products", method = RequestMethod.GET)
    List<Product> getAllProduct() {
        return productService.findAll();
    }

    @RequestMapping(value = "/Products", method = RequestMethod.POST)
    String addAllProducts(@RequestBody List<Product> ProductList) {
        productService.saveAll(ProductList);
        return "SUCCESS";
    }

    @RequestMapping(value = "/Products", method = RequestMethod.DELETE)
    String addAllProducts() {
        productService.deleteAll();
        return "SUCCESS";
    }
}
